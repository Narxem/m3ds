Agez Adrien
Tanghe Jérôme

# Partie 2
## Question 3
Dans l'éclairement par pixel, le sol est éclairé sur chaque pixel, ce qui n'est pas le cas dans l'éclairement par vertex, dans lequel le sol "n'est éclairé que sur les vertex"

# Partie 3
## Question 1
* La position de la texture est calculée par rapport à la position de la caméra
* Les coordonnées de texture doivent aller de 0 à 1. Ici, les coordonnées calculées vont de -1 à 1
* La texture nécessite un objet pour être affichée, mais il n'y en a pas dans le fond de la scène

## Question 4
* Les coordonnées de textures sont bien comprises entre 0 et 1. 

## Question 5
* Chaque objet possède sont propre repère, et donc ses propres coordonnées de texture
* L'image est projetée depuis la caméra


## Question 6 
* texCoord.z correspond à la profondeur du pixel de l'objet que l'on veut dessiner.
* fragColor.r correspond à la distance entre la source et le pixel de l'objet le plus proche