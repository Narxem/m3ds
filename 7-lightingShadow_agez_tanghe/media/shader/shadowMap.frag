#version 130

uniform sampler2D depthTexture;

in vec4 fTexCoord;

out vec4 fragColor;

void main() {
  vec4 texCoord = fTexCoord;


  texCoord.xyz /= texCoord.w; // clip coordinates => ndc

  texCoord = (texCoord + 1) /2;

  fragColor = texture(depthTexture, texCoord.xy);

  if (fragColor.r < texCoord.z-.001) {
      fragColor = vec4(0, 0, 0, 0.6);
  }
  else {
      fragColor = vec4(0, 0, 0, 0);
  }

  if (texCoord.x >= 1 || texCoord.x <= 0 || texCoord.y >= 1 || texCoord.y <= 0) {
      fragColor = vec4(0, 0, 0, 0);
  }

}
