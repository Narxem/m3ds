#version 130

out vec4 fragColor;
        
in vec4 fColor; // Calculée par interpolation des valeurs affectées
                // dans le vertex shader (varying)
in vec2 ftexCoord;

uniform sampler2D texture;


void main() {
    //fragColor = texture2D(texture, ftexCoord)*fColor;
     fragColor=texture2D(texture,ftexCoord)*fColor.b;
}
