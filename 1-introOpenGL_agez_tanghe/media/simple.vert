#version 130
        
in vec3 position; // x, y, z
in vec4 color;    // r, g, b, alpha
in vec2 texCoord; // Coordonées de textures

out vec2 ftexCoord;

uniform float coeff;

out vec4 fColor; // Varying
        
void main() {
    vec3 newPosition = position * coeff;
    fColor = color;
    ftexCoord = texCoord;
    gl_Position = vec4(newPosition,1.0);

    fColor.g*=(1.0-coeff);
}
