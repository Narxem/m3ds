AGEZ Adrien
TANGHE Jérôme

Tout fonctionne.

Réponse à la question 4 :
L'instabilité du mouvement se caractérise par un « tremblement » au niveau des
sphères situées à proximité des murs. Nous pensons que cela est dû à l'effet
combiné de la collision des sphères sur les murs ainsi qu'avec les sphères situées derrière.

Une autre possibilité sur l'origine de ce phénomène peut être le fait que la
collision des sphères avec les murs, calculés de la même manière qu'entre sphères
— en tenant compte de la négativité de la distance –, provoque, par repoussement
entre sphères, un effet de tremblements à proximité des murs.
